package com.example.ananddubey.musicplayer;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashSet;

/**
 * Created by Anand Dubey on 14/11/2016.
 */

public class ListAlbums extends Fragment {

    ArrayList<File> songslist=new ArrayList<File>();
    ArrayList<String> songslist1=new ArrayList<String>();
    //LinkedHashSet<String> albumslist=new LinkedHashSet<String>();
    ArrayList<DetailsAlbum> sendList=new ArrayList<DetailsAlbum>();
    ArrayList<String> albumslist=new ArrayList<String>();
    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;







    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.list_albums, container, false);



        System.out.println("ALBUMSSSSS KA ONCREATE CALL HUAA ///////////////////////");


        songslist=Main2Activity.songslist;
        songslist1=Main2Activity.songslist1;





        for(int i=0;i<songslist1.size();i++)
        {

            //Uri u=Uri.parse(songslist.get(i).toString());
            MediaMetadataRetriever mmr = new MediaMetadataRetriever();
            mmr.setDataSource(songslist1.get(i));

            String temp=(String)(mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM));
            if(!(albumslist.contains(temp))) {

                albumslist.add(temp);



                String tname = (String) (mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM));


                DetailsAlbum d = new DetailsAlbum(tname,tname);

                sendList.add(d);


            }


        }




        recyclerView=(RecyclerView)rootView.findViewById(R.id.recycler_view_albums);
        adapter=new RecyclerAdapterAlbums(sendList,container.getContext(),songslist1);
        recyclerView.setHasFixedSize(true);

        recyclerView.setAdapter(adapter);
        StaggeredGridLayoutManager gm=new StaggeredGridLayoutManager(3,StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(gm);





        return rootView;
    }

    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix,false);
        bm.recycle();
        return resizedBitmap;
    }




}



















class DetailsAlbum
{
    String img=null;
    String name=null;

    DetailsAlbum(String img, String name)
    {
        this.img=img;
        this.name=name;

    }

    public String getName() {
        return name;
    }


    public String getImg() {

        return img;
    }

}










class RecyclerAdapterAlbums extends RecyclerView.Adapter<RecyclerAdapterAlbums.RecyclerViewHolder1>{
    String axis="y";
    int prevposi=0;


    private ArrayList<DetailsAlbum> arrayList=new ArrayList<DetailsAlbum>();
    private ArrayList<String> fileList=new ArrayList<String>();
    Context ctx;

    public RecyclerAdapterAlbums(ArrayList<DetailsAlbum> arrayList,Context ctx,ArrayList<String> songslist)
    {



        this.fileList=songslist;

        this.arrayList=arrayList;
        this.ctx=ctx;

    }




    @Override
    public RecyclerViewHolder1 onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_menu_albums, parent, false);
        RecyclerViewHolder1 recyclerViewHolder=new RecyclerViewHolder1(view,ctx,arrayList);
        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder1 holder, int position) {

        DetailsAlbum d = arrayList.get(position);
        if (d.getName() != null) {
            holder.song_Name.setText(d.getName());
        }
        else
        holder.song_Name.setText("Untitled");

        if (d.getImg() == null) {
            holder.icon_image.setImageResource(R.mipmap.ic_launcher);
            } else {
                String path=Environment.getExternalStorageDirectory().getAbsolutePath()+"/AnandAlbums/Thumbnails/"+d.getImg()+".jpg";
                Uri u= Uri.parse(path);
                holder.icon_image.setImageURI(u);
               // Bitmap sh=getResizedBitmap(d.getImg(),100,100);
               // holder.icon_image.setImageResource(d.getImg());
            }


    }
    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix,false);

        return resizedBitmap;
    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }




    public class RecyclerViewHolder1 extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        TextView song_Name;
        ImageView icon_image;

        ArrayList<DetailsAlbum> arrayList=new ArrayList<DetailsAlbum>();
        Context ctx;

        public RecyclerViewHolder1(View itemView,Context ctx,ArrayList<DetailsAlbum> arrayList) {
            super(itemView);
            this.arrayList=arrayList;
            this.ctx=ctx;
            itemView.setOnClickListener(this);

            song_Name=(TextView)itemView.findViewById(R.id.textView_album_list_name);
            icon_image=(ImageView)itemView.findViewById(R.id.imageView_album_list_image);
        }

        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void onClick(View v) {

            Intent i=new Intent(this.ctx,AlbumsList.class);

            i.putExtra("pos",getAdapterPosition());
            i.putExtra("aName",arrayList.get(getAdapterPosition()).getName());

            ctx.startActivity(i);


        }
    }
}

