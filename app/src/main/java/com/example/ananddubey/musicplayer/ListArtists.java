package com.example.ananddubey.musicplayer;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;

/**
 * Created by Anand Dubey on 14/11/2016.
 */

public class ListArtists extends Fragment {


    ArrayList<File> songslist=new ArrayList<File>();

    //LinkedHashSet<String> albumslist=new LinkedHashSet<String>();
    ArrayList<DetailsAlbum> sendList=new ArrayList<DetailsAlbum>();
    ArrayList<String> artistlist=new ArrayList<String>();
    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;
    ArrayList<String> songslist1=new ArrayList<String>();


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.list_artists, container, false);



        System.out.println("ARTIST..... KA ONCREATE CALL HUAA ///////////////////////");


        songslist=Main2Activity.songslist;
        songslist1=Main2Activity.songslist1;
        Bitmap art = null,art1 = null;


        for(int i=0;i<songslist1.size();i++)
        {

            //Uri u=Uri.parse(songslist1.get(i));
            MediaMetadataRetriever mmr = new MediaMetadataRetriever();
            mmr.setDataSource(songslist1.get(i));


            String temp=(mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST));
            if(!(artistlist.contains(temp))) {

                artistlist.add(temp);



                DetailsAlbum d = new DetailsAlbum(Main2Activity.albums.get(i),Main2Activity.artists.get(i));

                sendList.add(d);


            }
            


        }




        recyclerView=(RecyclerView)rootView.findViewById(R.id.recycler_view_artists);
        adapter=new RecyclerAdapterAlbums(sendList,container.getContext(),songslist1);
        recyclerView.setHasFixedSize(true);

        recyclerView.setAdapter(adapter);
        StaggeredGridLayoutManager gm=new StaggeredGridLayoutManager(3,StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(gm);






        return rootView;
    }

}
