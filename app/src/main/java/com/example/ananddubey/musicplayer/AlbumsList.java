package com.example.ananddubey.musicplayer;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

public class AlbumsList extends AppCompatActivity {
    Intent i;
    String aName;
    ArrayList<String> sendList,songName;
    ListView listView;
    ArrayAdapter<String> arrayAdapter;
    ImageView imgMain;
    MediaMetadataRetriever mmr;
    Boolean imgset=true;
    byte[] rawArt;
    Bitmap art = null;
    BitmapFactory.Options bfo=new BitmapFactory.Options();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_albums_list);
        listView=(ListView)findViewById(R.id.ListView_Album_SongsList);

        imgMain=(ImageView)findViewById(R.id.ImageView_Album_Image_Main);
        sendList=new ArrayList<String>();
        songName=new ArrayList<String>();
        mmr=new MediaMetadataRetriever();

        i=getIntent();
        Bundle b=i.getExtras();

        aName=b.getString("aName");

        new AlbumAsyn().execute();




        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Intent s=new Intent(AlbumsList.this,Play_Music.class);
                s.putExtra("pos",i);
                s.putExtra("songslist",sendList);
                startActivity(s);

            }
        });

    }


    public void setAdapter()
    {
        arrayAdapter=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,songName);
        listView.setAdapter(arrayAdapter);

    }

    class AlbumAsyn extends AsyncTask<Void,Void,Void>
    {

        @Override
        protected Void doInBackground(Void... voids) {


            for(int i=0;i<Main2Activity.songslist1.size();i++) {
                mmr.setDataSource(Main2Activity.songslist1.get(i));
                if (mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM) != null)
                {
                    if (mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM).equals(aName)) {
                        songName.add(mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE));
                        sendList.add(Main2Activity.songslist1.get(i));

                        if(imgset)
                        {
                            rawArt = mmr.getEmbeddedPicture();
                            art = BitmapFactory.decodeByteArray(rawArt,0, rawArt.length, bfo);
                            publishProgress();
                        }
                    }
                }

            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            setAdapter();

        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
            if (null != rawArt) {
                imgMain.setImageBitmap(art);
            }
            else {
                imgMain.setImageResource(R.drawable.anand_icon);
            }
            imgset=false;

        }
    }
}
