package com.example.ananddubey.musicplayer;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.media.RatingCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.UrlConnectionDownloader;

import java.io.File;
import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * Created by Anand Dubey on 04/11/2016.
 */

class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.RecyclerViewHolder>{
    String axis="y";
    int prevposi=0;


    private ArrayList<Details> arrayList=new ArrayList<Details>();
    private ArrayList<String> fileList=new ArrayList<String>();
    Context ctx;

    public RecyclerAdapter(ArrayList<Details>arrayList,Context ctx,ArrayList<String> songslist)
    {

        this.axis=axis;

        this.fileList=songslist;

        this.arrayList=arrayList;
        this.ctx=ctx;

    }




    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_menu, parent, false);
        RecyclerViewHolder recyclerViewHolder=new RecyclerViewHolder(view,ctx,arrayList);
        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {

        Details d = arrayList.get(position);
        if (!(d.getName()== null || d.getName().equals("null"))) {
            holder.song_Name.setText(d.getName().replace(".mp3", ""));
            holder.artist_Name.setText(d.getArtist());
            holder.year.setText(d.getYear());
        }
        else
        {
            holder.song_Name.setText("Untitled");
            holder.artist_Name.setText("Untitled");
            holder.year.setText("Untitled");
        }

        if (!(d.getImg() == null || d.getImg().equals("null"))) {
            //Picasso.with(ctx).load(d.getImg()).into(holder.icon_image);
            String path=Environment.getExternalStorageDirectory().getAbsolutePath()+"/AnandAlbums/Thumbnails/"+d.getImg()+".jpg";
            Uri u= Uri.parse(path);
            holder.icon_image.setImageURI(u);
            //holder.icon_image.setImageResource(R.mipmap.ic_launcher);
        } else {
            holder.icon_image.setImageResource(R.drawable.anand_icon);
        }


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix,false);

        return resizedBitmap;
    }




    public class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        TextView song_Name,artist_Name,year;
        ImageView icon_image;

        ArrayList<Details> arrayList=new ArrayList<Details>();
        Context ctx;

        public RecyclerViewHolder(View itemView,Context ctx,ArrayList<Details> arrayList) {
            super(itemView);
            this.arrayList=arrayList;
            this.ctx=ctx;
            itemView.setOnClickListener(this);

            song_Name=(TextView)itemView.findViewById(R.id.textView_song_name);
            artist_Name=(TextView)itemView.findViewById(R.id.textView_artist);
            year=(TextView)itemView.findViewById(R.id.textView_year);
            icon_image=(ImageView)itemView.findViewById(R.id.imageView_song_image);
        }

        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void onClick(View v) {

            Intent i=new Intent(this.ctx,Play_Music.class);

            i.putExtra("pos",getAdapterPosition());
            i.putExtra("songslist",fileList);

            ctx.startActivity(i);


        }
    }
}


class Details
{
    String img=null;
    String name=null,artist=null;
    String year=null;
    Details(String img, String name, String artist, String year)
    {
        this.img=img;
        this.name=name;
        this.artist=artist;
        this.year=year;
    }

    public String getName() {
        return name;
    }

    public String  getYear() {
        return year;
    }

    public String getImg() {

            return img;
    }

    public String getArtist() {
        return artist;

    }
}
