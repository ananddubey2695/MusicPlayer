package com.example.ananddubey.musicplayer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Parcelable;
import android.provider.ContactsContract;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class Play_Music extends AppCompatActivity implements View.OnClickListener{

    ArrayList<String> songs=new ArrayList<String>();

    ImageButton play_button,nxt_button,prev_button,FF_button,FR_button,volume_Mute;
    ImageView album_image;
    TextView songname,artist,year;
    SeekBar sb;
    MediaPlayer mp;
    Uri u;
    Thread seek;
    int position;
    PlayService ps;
    Intent i;
    Boolean playing=false;
    AudioManager am;
    BroadcastReceiver br;
    IntentFilter IF;
    LocalBroadcastManager lbr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play__music);



        i=getIntent();
        Bundle b=i.getExtras();

        IF=new IntentFilter();
        IF.addAction("com.msg");


        br=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String s=intent.getStringExtra("show");
                String s1=intent.getStringExtra("total");
                String s2=intent.getStringExtra("repeat");
                int seektym = Integer.parseInt(s);
                int seektotal = Integer.parseInt(s1);
                sb.setMax(seektotal);
                sb.setProgress(seektym);


                //Toast.makeText(Play_Music.this,s,Toast.LENGTH_SHORT).show();
            }
        };
        //lbr=LocalBroadcastManager.getInstance(getApplicationContext());
        //lbr.registerReceiver(br,IF);
        registerReceiver(br,IF);


        int pos=b.getInt("pos");
        songs= (ArrayList) b.getParcelableArrayList("songslist");
        position=pos;

        play_button=(ImageButton)findViewById(R.id.imageButton_Play);
        FF_button=(ImageButton)findViewById(R.id.imageButton_FF);
        nxt_button=(ImageButton)findViewById(R.id.imageButton_Next);
        prev_button=(ImageButton)findViewById(R.id.imageButton_prev);
        FR_button=(ImageButton)findViewById(R.id.imageButton_FR);
        volume_Mute=(ImageButton)findViewById(R.id.imageButton_VolumeMute);
        album_image=(ImageView)findViewById(R.id.imageView_AlbumImage);
        songname=(TextView)findViewById(R.id.textView_SongName);
        artist=(TextView)findViewById(R.id.textView_ArtistName);
        year=(TextView)findViewById(R.id.textView_Playmusic_Year);
        sb=(SeekBar)findViewById(R.id.seekBar1);
        play_button.setOnClickListener(this);
        nxt_button.setOnClickListener(this);
        prev_button.setOnClickListener(this);
        FF_button.setOnClickListener(this);
        FR_button.setOnClickListener(this);
        volume_Mute.setOnClickListener(this);

        this.setVolumeControlStream(AudioManager.STREAM_MUSIC);



        i=new Intent(this,PlayService.class);
        stopService(i);

        i.putExtra("song",songs.get(position));
        startService(i);
        playing=true;
        play_button.setImageResource(R.drawable.pause);

        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        byte[] rawArt;
        Bitmap art = null;
        BitmapFactory.Options bfo=new BitmapFactory.Options();

        mmr.setDataSource(getApplicationContext(),Uri.parse(songs.get(position)));
        rawArt = mmr.getEmbeddedPicture();

        if (null != rawArt) {
            art = BitmapFactory.decodeByteArray(rawArt, 0, rawArt.length, bfo);
            album_image.setImageBitmap(art);
        }
        else
            album_image.setImageResource(R.drawable.anand_icon);



        songname.setText((String)(mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE)));
        artist.setText((String)(mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST)));
        year.setText((String)(mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_YEAR)));







sb.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        int time=seekBar.getProgress();
        i=new Intent(Play_Music.this,PlayService.class);
        i.putExtra("sw","sT");
        i.putExtra("seekTo",time);
        startService(i);

    }
});







    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(br);

    }



    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.imageButton_Play:

                if(playing==true)
                {
                    i=new Intent(this,PlayService.class);
                    i.putExtra("sw","pause");
                    startService(i);
                    play_button.setImageResource(R.drawable.play);
                    playing=false;

                }
                else {
                    i=new Intent(this,PlayService.class);
                    i.putExtra("song",songs.get(position));
                    startService(i);
                    play_button.setImageResource(R.drawable.pause);
                    playing=true;
                }
                break;

            case R.id.imageButton_Next:
                playing=true;
                play_button.setImageResource(R.drawable.pause);

                stopService(i);
                unregisterReceiver(br);
                position=(position+1)%songs.size();
                i=new Intent(this,PlayService.class);
                i.putExtra("song",songs.get(position));
                registerReceiver(br,IF);
                startService(i);

                MediaMetadataRetriever mmr = new MediaMetadataRetriever();
                byte[] rawArt;
                Bitmap art = null;
                BitmapFactory.Options bfo=null;
                bfo=new BitmapFactory.Options();

                mmr.setDataSource(getApplicationContext(),Uri.parse(songs.get(position)));
                rawArt = mmr.getEmbeddedPicture();

                if (null != rawArt) {
                    art = BitmapFactory.decodeByteArray(rawArt, 0, rawArt.length, bfo);
                    album_image.setImageBitmap(art);
                }
                else
                    album_image.setImageResource(R.drawable.anand_icon);



                songname.setText((String)(mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE)));
                artist.setText((String)(mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST)));
                year.setText((String)(mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_YEAR)));



                break;

            case R.id.imageButton_prev:
                playing=true;
                play_button.setImageResource(R.drawable.pause);
                stopService(i);
                unregisterReceiver(br);
                position=(position-1<0)? songs.size()-1:position-1;
                i=new Intent(this,PlayService.class);
                i.putExtra("song",songs.get(position));
                registerReceiver(br,IF);
                startService(i);



                art = null;
                bfo=null;
                bfo=new BitmapFactory.Options();
                mmr=new MediaMetadataRetriever();
                mmr.setDataSource(getApplicationContext(),Uri.parse(songs.get(position)));
                rawArt = mmr.getEmbeddedPicture();

                if (null != rawArt) {
                    art = BitmapFactory.decodeByteArray(rawArt, 0, rawArt.length, bfo);
                    album_image.setImageBitmap(art);
                }
                else
                    album_image.setImageResource(R.drawable.anand_icon);



                songname.setText((String)(mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE)));
                artist.setText((String)(mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST)));
                year.setText((String)(mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_YEAR)));


                break;

            case R.id.imageButton_FF:
                playing=true;
                play_button.setImageResource(R.drawable.pause);
                i=new Intent(this,PlayService.class);
                i.putExtra("sw","ff");
                startService(i);
                break;

            case R.id.imageButton_FR:
                playing=true;
                play_button.setImageResource(R.drawable.pause);
                i=new Intent(this,PlayService.class);
                i.putExtra("sw","fr");
                startService(i);
                break;

            case R.id.imageButton_VolumeMute:
                AudioManager am=(AudioManager)getSystemService(Context.AUDIO_SERVICE);


                if(am.getStreamVolume(AudioManager.STREAM_MUSIC)==0)
                {
                    am.setStreamVolume(AudioManager.STREAM_MUSIC,AudioManager.ADJUST_UNMUTE,AudioManager.FLAG_SHOW_UI);
                    volume_Mute.setImageResource(R.drawable.volumee);
                }
                else {
                    am.setStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_MUTE, AudioManager.FLAG_SHOW_UI);
                    volume_Mute.setImageResource(R.drawable.mute);
                }

                break;



        }

    }
}
