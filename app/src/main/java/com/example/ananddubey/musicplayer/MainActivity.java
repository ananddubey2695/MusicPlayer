package com.example.ananddubey.musicplayer;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class MainActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;
    ArrayList<String> songslist1=new ArrayList<String>();

    File f,f1;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        songslist1=Main2Activity.songslist1;







        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        100);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
        else
        {
            f=Environment.getExternalStorageDirectory();
            f1=Environment.getRootDirectory();

            ArrayList<File> songslist=listSongs(f);


            ArrayList<Details> sendlist=new ArrayList<Details>();

            for(int i=0;i<songslist.size();i++)
            {
                Uri u=Uri.parse(songslist.get(i).toString());
                MediaMetadataRetriever mmr = new MediaMetadataRetriever();
                byte[] rawArt;
                Bitmap art = null;
                BitmapFactory.Options bfo=new BitmapFactory.Options();

                mmr.setDataSource(getApplicationContext(), u);
                rawArt = mmr.getEmbeddedPicture();

                if (null != rawArt)
                    art = BitmapFactory.decodeByteArray(rawArt, 0, rawArt.length, bfo);
                else
                    art=null;


                String tname=(String)(mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE));
                String tartist=(String)(mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST));
                String tyear=(String)(mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_YEAR));

                Details d=new Details("HI",tname,tartist,tyear);
                sendlist.add(d);



            }


            recyclerView=(RecyclerView)findViewById(R.id.recycler_view);
            adapter=new RecyclerAdapter(sendlist,this,songslist1);
            recyclerView.setHasFixedSize(true);

            recyclerView.setAdapter(adapter);
            StaggeredGridLayoutManager gm=new StaggeredGridLayoutManager(1,StaggeredGridLayoutManager.VERTICAL);
            recyclerView.setLayoutManager(gm);
        }






















    }

    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 100: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    f=Environment.getExternalStorageDirectory();
                    f1=Environment.getRootDirectory();

                    ArrayList<File> songslist=listSongs(f);

                    ArrayList<Details> sendlist=new ArrayList<Details>();

                    for(int i=0;i<songslist.size();i++)
                    {
                        Uri u=Uri.parse(songslist.get(i).toString());
                        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
                        byte[] rawArt;
                        Bitmap art = null;
                        BitmapFactory.Options bfo=new BitmapFactory.Options();

                        mmr.setDataSource(getApplicationContext(), u);
                        rawArt = mmr.getEmbeddedPicture();

                        if (null != rawArt)
                            art = BitmapFactory.decodeByteArray(rawArt, 0, rawArt.length, bfo);
                        else
                            art=null;


                        String tname=(songslist.get(i).getName().toString());
                        String tartist=(String)(mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST));
                        String tyear=(String)(mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_YEAR));

                        Details d=new Details("HI",tname,tartist,tyear);
                        sendlist.add(d);



                    }


                    recyclerView=(RecyclerView)findViewById(R.id.recycler_view);
                    adapter=new RecyclerAdapter(sendlist,this,songslist1);
                    recyclerView.setHasFixedSize(true);

                    recyclerView.setAdapter(adapter);
                    StaggeredGridLayoutManager gm=new StaggeredGridLayoutManager(1,StaggeredGridLayoutManager.VERTICAL);
                    recyclerView.setLayoutManager(gm);

                } else {
                    Toast.makeText(this,"Permission not granted.",Toast.LENGTH_LONG).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }



    public ArrayList<File> listSongs(File root)
    {
        ArrayList<File> al=new ArrayList<File>();
        File[] f=root.listFiles();


            for(File temp:f)
            {
                if(temp.isDirectory() && !temp.isHidden())
                {
                    al.addAll(listSongs(temp));

                }
                else if(temp.getName().endsWith(".mp3") || temp.getName().endsWith(".wav"))
                {
                    al.add(temp);
                }
            }

        return al;
    }
}



