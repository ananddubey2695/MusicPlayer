package com.example.ananddubey.musicplayer;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.RunnableFuture;

/**
 * Created by Anand Dubey on 08/11/2016.
 */

public class PlayService extends Service {
    String songpath;
    MediaPlayer mp;
    Uri u;
    Boolean playing=false;
    Boolean newsong=true;
    int tym=0;
    Thread t1;








    @Override
    public IBinder onBind(Intent intent) {
        Toast.makeText(this,"CHALU BIND",Toast.LENGTH_SHORT).show();
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {




        Bundle b = intent.getExtras();
        if(newsong) {
            songpath = b.getString("song");
            u = Uri.parse(songpath.toString());
            mp = MediaPlayer.create(this, u);
            newsong=false;
            t1.start();
            mp.start();

        }
       else
        {
            String s="f";
            if(b.getString("sw")!=null)
            s=b.getString("sw");


            switch (s)
            {
                case "ff":
                    if(mp.getCurrentPosition()+5000<mp.getDuration())
                        mp.seekTo(mp.getCurrentPosition()+5000);
                    break;
                case "fr":
                    if(mp.getCurrentPosition()-5000>1000)
                        mp.seekTo(mp.getCurrentPosition()-5000);
                    break;
                case "pause":
                    mp.pause();
                    playing=false;
                    break;
                case "sT":
                    int sT=b.getInt("seekTo");
                    mp.seekTo(sT);
                    break;
                default:
                    mp.start();
                    playing=true;


            }

           /* if(b.getInt("ff")==5)
            {
                if(mp.getCurrentPosition()+5000<mp.getDuration())
                mp.seekTo(mp.getCurrentPosition()+5000);
            }
            else if(b.getInt("ff")==-5)
            {
                if(mp.getCurrentPosition()-5000>1000)
                mp.seekTo(mp.getCurrentPosition()-5000);
            }
            else
            {
                mp.pause();
                playing=false;

            } */

        }


        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mp.stop();
        mp.release();
        t1.interrupt();
        stopSelf();
    }

    @Override
    public void onCreate() {

        super.onCreate();

        t1=new Thread(new Runnable() {
            @Override
            public void run() {

                while(mp.getCurrentPosition()!=mp.getDuration()){

                    Intent i = new Intent();
                    tym=mp.getCurrentPosition();
                    i.putExtra("show",Integer.toString(tym));
                    i.putExtra("total",Integer.toString(mp.getDuration()));
                    i.setAction("com.msg");
                    sendBroadcast(i);
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        return;
                    }
                }


            }
        });

        newsong=true;


    }

}
