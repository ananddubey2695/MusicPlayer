package com.example.ananddubey.musicplayer;

import android.content.Intent;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.ArrayList;

public class ArtistsList extends AppCompatActivity {

    Intent i;
    String aName;
    ArrayList<String> sendList,songName;
    ListView listView;
    ArrayAdapter<String> arrayAdapter;
    ImageView imgMain;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artists_list);




        listView=(ListView)findViewById(R.id.ListView_Artist_SongList);
        imgMain=(ImageView)findViewById(R.id.ImageView_Artist_Image_Main);
        sendList=new ArrayList<String>();
        songName=new ArrayList<String>();

        i=getIntent();
        Bundle b=i.getExtras();

        aName=b.getString("aName");
        String path= Environment.getExternalStorageDirectory().getAbsolutePath()+"/AnandAlbums/Thumbnails/"+aName+".jpg";
        Uri u= Uri.parse(path);
        imgMain.setImageURI(u);



        MediaMetadataRetriever mmr=new MediaMetadataRetriever();


        for(int i=0;i<Main2Activity.songslist1.size();i++)
        {
            mmr.setDataSource(Main2Activity.songslist1.get(i));
            if(mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM).equals(aName))
            {
                songName.add(mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE));
                sendList.add(Main2Activity.songslist1.get(i));
            }

        }

        arrayAdapter=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,songName);
        listView.setAdapter(arrayAdapter);



    }
}
