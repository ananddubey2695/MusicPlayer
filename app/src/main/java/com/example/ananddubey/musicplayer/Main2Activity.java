package com.example.ananddubey.musicplayer;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BlurMaskFilter;
import android.graphics.Matrix;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.media.VolumeProviderCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

public class Main2Activity extends AppCompatActivity {

    File f,f1,file;
    File f2,fpics;
    File fpath;
    File fsongs;
    File falbums;
    File fartists;
    FileReader fis;
    FileWriter fos;
    BufferedReader br;
    BufferedWriter bw = null;
    public static ArrayList<File> songslist=new ArrayList<File>();
    public static ArrayList<String> songslist1=new ArrayList<String >();
    public static ArrayList<String> songs=new ArrayList<String >();
    public static ArrayList<String> albums=new ArrayList<String >();
    public static ArrayList<String> artists=new ArrayList<String >();
    ArrayList<String> imglist=new ArrayList<String>();
    ProgressDialog pb;
    String str="";


    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        pb=new ProgressDialog(Main2Activity.this);

        System.out.println("MAin KA ONCREATE CALL HUAA ///////////////////////");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.





        String path=Environment.getExternalStorageDirectory().getAbsolutePath()+"/AnandAlbums";

        //String pathsongs=Environment.getExternalStorageDirectory().getAbsolutePath()+"/AnandAlbums";
        //String pathalbums=Environment.getExternalStorageDirectory().getAbsolutePath()+"/AnandAlbums";
        //String pathartists=Environment.getExternalStorageDirectory().getAbsolutePath()+"/AnandAlbums";


        f= Environment.getExternalStorageDirectory();
        f1=Environment.getRootDirectory();

        f2=new File(path);
        fpath=new File(path,"SongsPath.txt");
        fsongs=new File(path,"SongsList.txt");
        falbums=new File(path,"AlbumsList.txt");
        fartists=new File(path,"ArtistsList.txt");
        fpics=new File(path,"Thumbnails");
        file = new File(path+"/Thumbnails",("SomeImage.jpg")); // the File to save.





        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(Main2Activity.this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.


                ActivityCompat.requestPermissions(Main2Activity.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        100);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
        else
        {

            new DoTask().execute();

        }




    }








    public void mkfolders()
    {


        if(!f2.exists())
        {
            f2.mkdir();
        }



        try {

            if(!fpath.exists())
            {
                fpath.createNewFile();
                try {
                    fos=new FileWriter(fpath);
                    bw=new BufferedWriter(fos);
                } catch (IOException e) {
                    e.printStackTrace();
                }



                songslist=listSongs(f);

                for(int i=0;i<songslist.size();i++)
                {
                    try {
                        bw.write(songslist.get(i).toString()+"fif");

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                try {
                    bw.flush();
                    fos.flush();

                } catch (IOException e) {
                    e.printStackTrace();
                }


            }

            if(!fsongs.exists())
            {

                fsongs.createNewFile();
                try {
                    fos=new FileWriter(fsongs);
                    bw=new BufferedWriter(fos);
                } catch (IOException e) {
                    e.printStackTrace();
                }


                for(int i=0;i<songslist.size();i++)
                {
                    MediaMetadataRetriever mmr = new MediaMetadataRetriever();
                    mmr.setDataSource(songslist.get(i).toString());
                    try {
                        bw.write((mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE))+"fif");

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                try {
                    bw.flush();
                    fos.flush();

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            if(!falbums.exists())
            {
                falbums.createNewFile();
                try {
                    fos=new FileWriter(falbums);
                    bw=new BufferedWriter(fos);
                } catch (IOException e) {
                    e.printStackTrace();
                }


                for(int i=0;i<songslist.size();i++)
                {
                    MediaMetadataRetriever mmr = new MediaMetadataRetriever();
                    mmr.setDataSource(songslist.get(i).toString());
                    try {
                        imglist.add(mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM));
                        bw.write((mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM))+"fif");

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                try {
                    bw.flush();
                    fos.flush();

                } catch (IOException e) {
                    e.printStackTrace();
                }




            }

            if(!fartists.exists())
            {
                fartists.createNewFile();
                try {
                    fos=new FileWriter(fartists);
                    bw=new BufferedWriter(fos);
                } catch (IOException e) {
                    e.printStackTrace();
                }


                for(int i=0;i<songslist.size();i++)
                {
                    MediaMetadataRetriever mmr = new MediaMetadataRetriever();
                    mmr.setDataSource(songslist.get(i).toString());
                    try {
                        bw.write((mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST))+"fif");

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                try {
                    bw.flush();
                    fos.flush();

                } catch (IOException e) {
                    e.printStackTrace();
                }



            }






        } catch (IOException e) {
            e.printStackTrace();
        }


    }






    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 100: {
                // If request is cancelled, the result arrays are empty.

                new DoTask().execute();


                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }





    public ArrayList<File> listSongs(File root)
    {
        ArrayList<File> al=new ArrayList<File>();
        File[] f=root.listFiles();


        for(File temp:f)
        {
            if(temp.isDirectory() && !temp.isHidden())
            {
                al.addAll(listSongs(temp));

            }
            else if(temp.getName().endsWith(".mp3"))
            {
                al.add(temp);
            }
        }

        return al;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {        public SectionsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

        @Override
        public Fragment getItem(int position) {
            switch (position)
            {
                case 0:
                    ListSongs tab1=new ListSongs();
                    return tab1;

                case 1:
                    ListAlbums tab2=new ListAlbums();
                    return tab2;

                case 2:
                    ListArtists tab3=new ListArtists();
                    return tab3;

                default:
                    return null;
            }

        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Songs";
                case 1:
                    return "Albums";
                case 2:
                    return "Artists";
            }
            return null;
        }


    }

    class DoTask extends AsyncTask<Void,Void,Void>
    {

        @Override
        protected Void doInBackground(Void... params) {

            mkfolders();
            try {
                grabinfo();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPreExecute() {


            pb.setMessage("Loading...");
            pb.show();


        }

        @Override
        protected void onPostExecute(Void aVoid) {

            if(!fpics.exists())
            {
                fpics.mkdir();


                for(int i=0;i<imglist.size();i++)
                {
                    File fnow=new File(fpics,imglist.get(i)+".jpg");

                    if(!fnow.exists())
                    {
                        try {
                            fnow.createNewFile();

                            OutputStream fOut;


                            MediaMetadataRetriever mmr = new MediaMetadataRetriever();
                            byte[] rawArt=null;

                            mmr.setDataSource(songslist.get(i).toString());

                            rawArt = mmr.getEmbeddedPicture();
                            Bitmap art1;

                            if(rawArt!=null) {
                                art1 = BitmapFactory.decodeByteArray(rawArt, 0, rawArt.length);
                                if (art1 != null) {

                                    fOut = new FileOutputStream(fnow);

                                    art1.compress(Bitmap.CompressFormat.JPEG, 10, fOut); // saving the Bitmap to a file compressed as a JPEG with 85% compression rate
                                    fOut.flush(); // Not really required

                                    MediaStore.Images.Media.insertImage(getContentResolver(), fnow.getAbsolutePath(), fnow.getName(), fnow.getName());

                                }
                            }


                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }






                /*
                for(int i=0;i<songslist.size();i++)
                {


                    MediaMetadataRetriever mmr = new MediaMetadataRetriever();
                    byte[] rawArt=null;

                    mmr.setDataSource(songslist.get(i).toString());

                    rawArt = mmr.getEmbeddedPicture();
                    Bitmap art1;
                    if (null != rawArt) {

                        art1 = BitmapFactory.decodeByteArray(rawArt,0, rawArt.length);

                        fOut = new FileOutputStream(file);

                        art1.compress(Bitmap.CompressFormat.JPEG, 50, fOut); // saving the Bitmap to a file compressed as a JPEG with 85% compression rate
                        fOut.flush(); // Not really required

                        MediaStore.Images.Media.insertImage(getContentResolver(),file.getAbsolutePath(),file.getName(),file.getName());


                    }




                }
                */






            }










            if(pb.isShowing())
            {
                pb.dismiss();
            }
            System.out.println("List Making OVER////////////////////");

            mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

            // Set up the ViewPager with the sections adapter.

            mViewPager = (ViewPager) findViewById(R.id.container);
            mViewPager.setAdapter(mSectionsPagerAdapter);

            TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
            tabLayout.setupWithViewPager(mViewPager);
            Toast.makeText(Main2Activity.this,"List Updated",Toast.LENGTH_LONG).show();





        }

        @Override
        protected void onProgressUpdate(Void... values) {

        }

        public void grabinfo() throws IOException {

            //songslist=listSongs(f);

            fis=new FileReader(fpath);
            br=new BufferedReader(fis);
            String s;

            while ((s=br.readLine())!=null)
            {
                str+=(s);

            }
            System.out.println(str);

            for(String s1:str.split("fif"))
            {
                songslist1.add(s1);


            }



            System.out.println("//////////////////////////// Heres d list");









            str="";


            try {
                fis=new FileReader(fsongs);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }


            br=new BufferedReader(fis);


            try {
                while ((s=br.readLine())!=null)
                {
                    str+=(s);

                }
            } catch (IOException e) {
                e.printStackTrace();
            }


            for(String s1:str.split("fif"))
            {
                songs.add(s1);

            }
            System.out.println("//////////////////////////// Heres d list");










            str="";


            try {
                fis=new FileReader(falbums);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }


            br=new BufferedReader(fis);


            try {
                while ((s=br.readLine())!=null)
                {
                    str+=(s);

                }
            } catch (IOException e) {
                e.printStackTrace();
            }


            for(String s1:str.split("fif"))
            {
                albums.add(s1);

            }
            System.out.println("//////////////////////////// Heres d list");









            str="";


            try {
                fis=new FileReader(fartists);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }


            br=new BufferedReader(fis);


            try {
                while ((s=br.readLine())!=null)
                {
                    str+=(s);

                }
            } catch (IOException e) {
                e.printStackTrace();
            }


            for(String s1:str.split("fif"))
            {
                artists.add(s1);

            }
            System.out.println("//////////////////////////// Heres d list");






        }
    }

}
