package com.example.ananddubey.musicplayer;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.media.VolumeProviderCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Anand Dubey on 14/11/2016.
 */

public class ListSongs extends Fragment {

    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;
    ArrayList<String> songslist1=new ArrayList<String>();
    ArrayList<File> songslist=new ArrayList<File>();
    ArrayList<Details> sendlist=new ArrayList<Details>();

    FileReader fis;
    FileWriter fos;
    BufferedReader br;
    BufferedWriter bw;
    File songsfile;
    File fpath;
    String str="";
    ProgressDialog pd;
    View rootView;







    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.list_songs, container, false);


        sendlist.clear();
        System.out.println("LIST SONGGGG KA ONCREATE CALL HUAA ///////////////////////");
        String path=Environment.getExternalStorageDirectory().getAbsolutePath()+"/AnandAlbums/SongsList.txt";
        fpath=new File(path);
        //songslist=Main2Activity.songslist;
        songslist1=Main2Activity.songslist1;
        pd=new ProgressDialog(inflater.getContext());
        pd.setMessage("Loading...");




        new AsynSongList().execute(rootView);




/*

        for(int i=0;i<songslist1.size();i++)
        {

            //Uri u=Uri.parse(songslist1.get(i));
            MediaMetadataRetriever mmr = new MediaMetadataRetriever();
            mmr.setDataSource(songslist1.get(i));
            byte[] rawArt;
            ByteArrayOutputStream out=new ByteArrayOutputStream();
            Bitmap art = null,art1=null;
            BitmapFactory.Options bfo=new BitmapFactory.Options();


            rawArt = mmr.getEmbeddedPicture();



            if (null != rawArt) {
                bfo.inSampleSize = 4;
                art1 = BitmapFactory.decodeByteArray(rawArt,0, rawArt.length, bfo);

                   // for 1/2 the image to be loaded



                //art1=getScaledBitmap(art,100,100);
                //art1=getResizedBitmap(art,80,80);
                //art.recycle();
            }
            else {
                art1 = null;
            }






            String tname=(String)(mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE));
            String tartist=(String)(mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST));
            String tyear=(String)(mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_YEAR));





            Details d=new Details(art1,tname,tartist,tyear);
            sendlist.add(d);


        recyclerView=(RecyclerView)rootView.findViewById(R.id.recycler_view);
        adapter=new RecyclerAdapter(sendlist,container.getContext(),songslist1);
        recyclerView.setHasFixedSize(true);

        recyclerView.setAdapter(adapter);
        StaggeredGridLayoutManager gm=new StaggeredGridLayoutManager(1,StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(gm);


        }
        */














        return rootView;


    }



    public Bitmap getScaledBitmap(Bitmap b, int reqWidth, int reqHeight)
    {
        int bWidth = b.getWidth();
        int bHeight = b.getHeight();

        int nWidth = reqWidth;
        int nHeight = reqHeight;

        float parentRatio = (float) reqHeight / reqWidth;

        nHeight = bHeight;
        nWidth = (int) (reqWidth * parentRatio);

        return Bitmap.createScaledBitmap(b, nWidth, nHeight, true);
    }


    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix,true);
        bm.recycle();
        return resizedBitmap;
    }

    public void pst(View view)
    {
        recyclerView=(RecyclerView)view.findViewById(R.id.recycler_view);
        adapter=new RecyclerAdapter(sendlist,getContext(),songslist1);
        recyclerView.setHasFixedSize(true);

        recyclerView.setAdapter(adapter);
        StaggeredGridLayoutManager gm=new StaggeredGridLayoutManager(1,StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(gm);


    }




    class AsynSongList extends AsyncTask<View,View,View>
    {



        protected View doInBackground(View... parram) {


            byte[] rawArt = new byte[0];
            MediaMetadataRetriever mmr = new MediaMetadataRetriever();


            for(int i=0;i<Main2Activity.songs.size();i++)
            {
                mmr.setDataSource(songslist1.get(i));


                Details d=new Details(Main2Activity.albums.get(i),Main2Activity.songs.get(i),Main2Activity.artists.get(i),mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_YEAR));
                sendlist.add(d);


            }
            mmr.release();

            publishProgress(parram[0]);

            return null;
        }

        @Override
        protected void onPostExecute(View view) {
            pd.dismiss();


        }

        @Override
        protected void onProgressUpdate(View... values) {

            pst(values[0]);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd.show();
        }
    }






}
